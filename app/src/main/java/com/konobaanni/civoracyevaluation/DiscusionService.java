package com.konobaanni.civoracyevaluation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by bozidarkokot on 12/07/2017.
 */

public class DiscusionService {
    public ArrayList<DiscusionDataModel> getDiscusions(final Context context){
        final DBHelper dbHelper = new DBHelper(context);
        final ArrayList<DiscusionDataModel> discusionDataModels = new ArrayList<>();

        final StringRequest getRequest = new StringRequest(Request.Method.GET, "https://admin.civocracy.org/api/issues?filters[community]=384&filters[official]=1&order_by[date]=desc", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray array = jsonObject.getJSONArray("issues");


                    for(int i=0;i<array.length();i++){
                        JSONObject jsonObject1 = array.getJSONObject(i);


                        String base_url = "http://res-1.cloudinary.com/civocracy/image/upload/";

                        dbHelper.addDiscusiton(jsonObject1.getInt("id"),jsonObject1.getString("tag"),
                                jsonObject1.getString("title"),jsonObject1.getString("summary"),base_url+jsonObject1.getString("image"));


                        if(i==array.length()-1){
                            Intent intent = new Intent(context, DiscusionsActivity.class);

                             context.startActivity(intent);
                             ((Activity) context).finish();
                        }


                        // institutionEncapsulation = new InstitutionEncapsulation(imageUrl,context,lat,longitude,description,inst_name);
                        //institutionEncapsulation.execute(imageUrl);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //  context.startActivity(intent);
                    // ((Activity) context).finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("server resp: ", "" + error);
                //  context.startActivity(intent);
                // ((Activity) context).finish();


            }
        });
        RequestApp requestApp = new RequestApp();
        requestApp.setContext(context);
        requestApp.getInstance();
        requestApp.addToReqQueue(getRequest);

        return  discusionDataModels;
        // }
    }


}
