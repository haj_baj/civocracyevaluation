package com.konobaanni.civoracyevaluation;

/**
 * Created by bozidarkokot on 12/07/2017.
 */

public class DiscusionDataModel {
    public int id = 0;
    public String tag = "";
    public String title = "";
    public String summary = "";
    public  String image = "";

    public  DiscusionDataModel(int id,String tag,String title,String summary,String image){
        this.id = id;
        this.tag = tag;
        this.title = title;
        this.summary = summary;
        this.image = image;


    }


    public String getTitle(){
        return title;
    }
    public String getSummary(){
        return  summary;
    }
    public String getImage(){
        return  image;
    }
    public String  getTag(){
        return  tag;
    }
}
