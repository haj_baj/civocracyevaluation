package com.konobaanni.civoracyevaluation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by bozidarkokot on 12/07/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

        /**
         * SQLite Databases, categories data and businesses data
         * */

        public static final String databaseName = "discusions";
        public static final int databaseVersion = 13;




        public enum databaseTable{
            DISCUSIONS("discusions");
            public String table;
            databaseTable(String table) {
                this.table = table;
            }
        }


        public enum discusionColumn{
            ID("id"),
            TAG("tag"),
            TITLE("title"),
            SUMMARY("summary"),
            IMAGE("image");
            public  String column;
            discusionColumn(String column){
                this.column = column;
            }
        }













        public DBHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
    }

        @Override
        public void onCreate(SQLiteDatabase db) {


        String createDiscusionsTable = "CREATE TABLE "+databaseTable.DISCUSIONS.table+
                "("+discusionColumn.ID.column+" INTEGER PRIMARY KEY, "+
                discusionColumn.TAG.column+" VARCHAR, "+
                discusionColumn.SUMMARY.column+" VARCHAR, "+

                discusionColumn.TITLE.column+" VARCHAR," +
                discusionColumn.IMAGE.column+" VARCHAR);";

        db.execSQL(createDiscusionsTable);




    }



    public boolean addDiscusiton(int id,String tag,String summary,String title,String image){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(discusionColumn.ID.column, id);
        contentValues.put(discusionColumn.TAG.column, tag);
        contentValues.put(discusionColumn.SUMMARY.column, summary);
        contentValues.put(discusionColumn.TITLE.column, title);
        contentValues.put(discusionColumn.IMAGE.column, image);

        db.replace(databaseTable.DISCUSIONS.table, null, contentValues);
        db.close();
        return true;
    }








    public ArrayList<DiscusionDataModel> getAllDiscount(){
        ArrayList<DiscusionDataModel> discusionDataModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String readCategoriesSQL = "SELECT * FROM "+
                databaseTable.DISCUSIONS.table;
        Cursor cursor = db.rawQuery(readCategoriesSQL, null);
        if(cursor.moveToFirst()){
            while(!cursor.isAfterLast()){

                int id = cursor.getInt(cursor.getColumnIndex(discusionColumn.ID.column));
                String tag = cursor.getString(cursor.getColumnIndex(discusionColumn.TAG.column));
                String title = cursor.getString(cursor.getColumnIndex(discusionColumn.TITLE.column));
                String summary = cursor.getString(cursor.getColumnIndex(discusionColumn.SUMMARY.column));
                String image = cursor.getString(cursor.getColumnIndex(discusionColumn.IMAGE.column));




                discusionDataModels.add(new DiscusionDataModel(id,tag,title,summary,image));
                cursor.moveToNext();
            }
        }

        cursor.close();


        return discusionDataModels;
    }













    //A class which fills OrderData class instance which the result from sql query



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String dropTable = "DROP TABLE IF EXISTS "+databaseTable.DISCUSIONS.table;




        db.execSQL(dropTable);


        onCreate(db);
    }

}
