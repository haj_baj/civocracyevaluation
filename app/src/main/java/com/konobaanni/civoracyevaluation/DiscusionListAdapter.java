package com.konobaanni.civoracyevaluation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.microedition.khronos.opengles.GL;

/**
 * Created by bozidarkokot on 12/07/2017.
 */

public class DiscusionListAdapter extends ArrayAdapter<DiscusionDataModel> {

    private int layoutResource;

    public DiscusionListAdapter(Context context, int layoutResource, List<DiscusionDataModel> threeStringsList) {
        super(context, layoutResource, threeStringsList);
        this.layoutResource = layoutResource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(R.layout.discusion_list_item, null);
        }

        DiscusionDataModel discusionDataModel = getItem(position);

        if (discusionDataModel != null) {

            ImageView discusionImage = (ImageView) view.findViewById(R.id.discusion_image);
            TextView disusionName = (TextView) view.findViewById(R.id.discusion_name);
            TextView discusionDescription = (TextView) view.findViewById(R.id.discusion_desc);


            Picasso.with(getContext())
                    .load(discusionDataModel.getImage())
                    .placeholder(android.R.drawable.ic_dialog_alert)
                    .error(android.R.drawable.ic_dialog_alert)
                    .into(discusionImage);

            disusionName.setText(discusionDataModel.getTag());
            discusionDescription.setText(discusionDataModel.getSummary());



        }

        return view;
    }

}