package com.konobaanni.civoracyevaluation;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import java.util.List;

public class DiscusionsActivity extends AppCompatActivity {
    DiscusionListAdapter discusionListAdapter;
    DBHelper dbHelper;
    List<DiscusionDataModel> discusionDataModelList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discusions);
        ListView discusionList = (ListView) findViewById(R.id.discusion_list);

        dbHelper = new DBHelper(this);
        discusionDataModelList = dbHelper.getAllDiscount();

        discusionListAdapter = new DiscusionListAdapter(this,R.layout.discusion_list_item,discusionDataModelList);
        discusionList.setAdapter(discusionListAdapter);
    }

}
